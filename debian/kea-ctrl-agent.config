#!/bin/sh

set -e

. /usr/share/debconf/confmodule

ask_for_password() {
    while :; do
        RET=""
        db_input high kea-ctrl-agent/kea_api_password || true
        db_go || true
        db_get kea-ctrl-agent/kea_api_password
        if [ -z "$RET" ]; then
            # empty passwords result in no action
            break
        fi
        API_PASSWORD="$RET"
        db_input high kea-ctrl-agent/kea_api_password_again || true
        db_go || true
        db_get kea-ctrl-agent/kea_api_password_again
        if [ "$RET" = "$API_PASSWORD" ]; then
            API_PASSWORD=""
            break
        fi
        db_fset kea-ctrl-agent/password_mismatch seen false
        db_input critical kea-ctrl-agent/password_mismatch || true
        db_set kea-ctrl-agent/kea_api_password ""
        db_set kea-ctrl-agent/kea_api_password_again ""
        db_go || true
    done
}

gen_random_pw() {
    head -c 15 /dev/urandom | base64 | tr -d '[:space:]'
}


RET=""
choice=""
reconfigure=""

if [ "${1}" = "configure" ] || [ "${1}" = "reconfigure" ]; then
    if [ "${1}" = "reconfigure" ] || [ -n "${DEBCONF_RECONFIGURE}" ]; then
        reconfigure="yes"
    fi
    # only ask questions on:
    # - reconfigure
    # - fresh install
    # - upgrade from pre-debconf package (lt: empty version is "earlier", so
    #   this covers the fresh install case too)
    if [ -n "${reconfigure}" ] || dpkg --compare-versions "$2" lt "2.2.0-5ubuntu2~"; then
        db_input high kea-ctrl-agent/make_a_choice || true
        db_go || true

        db_get kea-ctrl-agent/make_a_choice
        choice="${RET}"

        case "${choice}" in
            unconfigured)
                # nothing to do
            ;;
            configured_password)
                ask_for_password
            ;;
            configured_random_password)
                db_set kea-ctrl-agent/kea_api_password "$(gen_random_pw)"
            ;;
            *)
                # shouldn't happen, so it's the same as "unconfigured" above
            ;;
        esac
    fi
fi
